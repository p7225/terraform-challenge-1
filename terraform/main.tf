terraform {
  required_version = ">= 1.1.7"
}

# dummy tf for Parbol example
output "hello_world" {
  value = "Hello, World! (Eternal message from early C programming book from Ritchie & Kernigham). Download a book here: https://www.engr.colostate.edu/ECE251/References/The%20C%20Programming%20Language.pdf"
}