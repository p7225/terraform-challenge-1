# Parbol challenge 1

Challenge described as: 

Create a terraform template to execute the following via a GitLab CI yml configuration:
  1. Init
  2. Validate
  3. Build
  4. Deploy


# Terraform gitlab CI template

In directory `templates` is sample terraform template - it uses only one Gitlab repo variable `CI_TERRAFORM_PROJECT_DIR`.
Variable is set in repository CI/CD settings/variables. 

`gitlab-ci.yaml` uses this template to execute terrraform code in directory `terraform`

